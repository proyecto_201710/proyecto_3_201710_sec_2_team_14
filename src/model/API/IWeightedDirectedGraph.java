package model.API;

import java.util.Iterator;
import java.util.NoSuchElementException;

import model.data_structures.NodoCamino;

public interface IWeightedDirectedGraph <K , V> {

	/**
	 * Retornar el n�mero de v�rtices
	 * @return el n�mero de v�rtices
	 */

	int numVertices();

	/**
	 * Consultar la informaci�n de un v�rtice dado su identificador.
	 * Si el identificador no existe se retorna la excepci�n
	 * �NoSuchElementException�.
	 * @param id del vertice
	 * @return el v�rtice con el identificador buscado
	 * @throws NoSuchElementException si no encuentra el elemento
	 * solicitado.
	 */
	
	V darVertice(K id) throws NoSuchElementException;

	
	/**
	 * Agregar un v�rtice con identificador �nico y su informaci�n
	 * asociada. Si el identificador ya existe, se reemplaza la
	 * informaci�n asociada al v�rtice.
	 * @param id
	 * @param indoVer
	 */
	void agregarVertice(K id, V indoVer);
	
	int numArcos();
	
	double darPesoArco(K idOrigen, K idDestino) throws NoSuchElementException;
	
	void agregarArco(K idOrigen, K idDestino, double peso);
	
	void agregarArco(K idOrigen, K idDestino, double peso, char ordenLexicografico);
	
	Iterator<K> darVertices();
	
	int darGrado(K id);
	
	Iterator<K> darVerticesAdyacentes(K id);
	
	void desmarcar();
	
	NodoCamino<K>[] DFS(K idOrigen);
	
	NodoCamino<K>[] BFS(K idOrigen);
	
	NodoCamino<K>[] DFS(K idOrigen, K idDestino);
	
	NodoCamino<K>[] BFS(K idOrigen, K idDestino);
	
	
	
	
	

}
