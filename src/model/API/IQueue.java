package model.API;

public interface IQueue <T> {

	/**
	 * Agrega un item en la ultima posici�n de la cola
	 * @param element
	 */
	void enqueue(T element);
	
	/**
	 * Elimina el elemento de la primera posici�n de la fila
	 * @return elemento de la primera posici�n de la fila
	 */
	T dequeue();
	
	/**
	 * Indica si la fila est� vac�a
	 * @return true si la fila est� vac�a, false en caso contrario.
	 */
	boolean isEmpty();
	
	
	/**
	 * Returna el numero de elementos en la fila.
	 * @return n�mero de elementos en la fila.
	 */
	int size();
	
	
}
