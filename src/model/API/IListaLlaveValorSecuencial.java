package model.API;

import java.util.Iterator;

public interface IListaLlaveValorSecuencial<K,V> {

	/**
	 * Devuelve el numero de llaves en la lista.
	 * @return numero de llaves en lista.
	 */
	public int darTamanio();
	
	/**
	 * Informa si la lista esta vacia o no.
	 * @return true si esta vacia. false de lo contrario.
	 */
	public boolean estaVacia();
	
	/**
	 * Informa si hay un elemento en la lista con la llave dada por parametro.
	 * @param llave != null. llave a buscar en la lista.
	 * @return true si hay elemento con llave dada. false de lo contrario.
	 */
	public boolean tieneLlave(K llave);
	
	/**
	 * Devuelve el valor asociado a la llave, devuelve null si no existe la llave en la lista.
	 * @param llave !=null. llave a buscar.
	 * @return valor asociado a llave.
	 */
	public V darValor(K llave);
	
	/**
	 * Inserta en la lista la llave y el valor.
	 * Si el valor es null y la llave existe debe eliminar elnodo de la lista.
	 * Si la llave no existe debe aniadir a la lista un nuevo nodo.
	 * Si la llave existe debe reemplazar el valosr del nodo.
	 * @param llave
	 * @param valor
	 */
	public void insertar(K llave, V valor);
	
	/**
	 * Devuelve un iterador sobre las llaves.
	 * @return Iterador.
	 */
	public Iterable<K> llaves();

	
}
