package model.API;

public interface IEncadenamientoSeparadoTH<K,V> {

	/**
	 * Crea una nueva tabla para disminuir o aumentar (si es el caso) el factor de carga
	 */
	public void reHash();
	
	
	/**
	 * Devuelve el factor de carga de la tabla de hash.
	 * @return factor de carga. 
	 */
	public double darFactorCarga();
	
	/**
	 * Devuelve el numero de llaves en la tabla.
	 * @return numero de llaves en tabla.
	 */
	public int darTamanio();
	
	/**
	 * Informa si la tabla esta vacia o no.
	 * @return true si esta vacia. false de lo contrario.
	 */
	public boolean estaVacia();
	
	/**
	 * Informa si hay un elemento en la tabla con la llave dada por parametro.
	 * @param llave != null. llave a buscar en la tabla.
	 * @return true si hay elemento con llave dada. false de lo contrario.
	 */
	public boolean tieneLlave(K llave);
	
	/**
	 * Devuelve el valor asociado a la llave, devuelve null si no existe la llave en la tabla.
	 * @param llave !=null. llave a buscar.
	 * @return valor asociado a llave.
	 */
	public V darValor(K llave);
	
	/**
	 * Inserta la llave y el valor en la tabla.
	 * Si el valor es null y la llave existe debe eliminar el elemento de la tabla.
	 * Si la llave existe debe reemplazar el valor asociado.
	 * Recuerde reducir las posiciones de la tabla o aumentarlas (rehash) dependiendo del factor de carga de la tabla. (n/m) 
	 * @param llave
	 * @param valor
	 */
	public void insertar(K llave, V valor);
	
	/**
	 * Devuelve un iterador sobre las llaves.
	 * @return Iterador.
	 */
	public Iterable<K> llaves();
	
	/**
	 * Devuelve la posici�}on de 0 a m-1 indicada para guardar la llave en la tabla.
	 * @param llave posicion para guardar la llave en la tabla.
	 * @return
	 */
	int hash(K llave);
	
	/**
	 * Devuelve la longitud de cada una de las listas que pertenecen a la tabla de hash.
	 * @return arreglo con longitud de cada lista.
	 */
	int[] darLongitudListas();
}
