
package model.VO;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOTeatro {

	/**
	 * Atributo que modela el nombre del teatro
	 */

	private String nombre;

	/**
	 * Atributo que modela la ubicacion del teatro 
	 */

	private VOUbicacion ubicacion;

	/**
	 * Atributo que referencia la franquicia
	 */

	private VOFranquicia franquicia;


	public VOTeatro() {

	}


	public String getNombre() {
		return nombre;
	}


	public VOUbicacion getUbicacion() {
		return ubicacion;
	}


	public VOFranquicia getFranquicia() {
		return franquicia;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setUbicacion(VOUbicacion ubicacion) {
		this.ubicacion = ubicacion;
	}

	public void setFranquicia(VOFranquicia franquicia) {
		this.franquicia = franquicia;
	}


	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return " Teatro: " + nombre + " Ubicacion: " + ubicacion + " " + franquicia; 
	}




}
