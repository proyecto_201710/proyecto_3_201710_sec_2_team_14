package model.VO;

import java.util.Arrays;

public class VOPelicula {
	
	private int id;
	private String titulo;
	private String[] generos;
	
	
	
	public VOPelicula(int id, String titulo, String[] generos) {
	
		this.id = id;
		this.titulo = titulo;
		this.generos = generos;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String[] getGeneros() {
		return generos;
	}
	public void setGeneros(String[] generos) {
		this.generos = generos;
	}
	
	@Override
	public String toString() {
		return "Id: " + getId() + " Titulo: " + getTitulo() + " Generos: " + Arrays.toString(getGeneros());
	}
	
	

}
