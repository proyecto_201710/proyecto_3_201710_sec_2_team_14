package model.VO;

public class PruebaVO implements Comparable<PruebaVO>{

	private String nombre;
	private int agno;
	@Override


	public int compareTo(PruebaVO pruebaVO) {
		// TODO Auto-generated method stub
		if(agno > pruebaVO.agno)
			return 1;

		else if(agno < pruebaVO.agno)
			return -1;

		else{
			if(nombre.compareTo(pruebaVO.nombre) > 0)
				return 1;

			else if (nombre.compareTo(pruebaVO.nombre) < 0)
				return -1;
			
			else
				return 0;
		}
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getAgno() {
		return agno;
	}
	public void setAgno(int agno) {
		this.agno = agno;
	}
	
	@Override
	public String toString() {
		return "Agno: " + agno + " Nombre: " + nombre + ".";
	}

}
