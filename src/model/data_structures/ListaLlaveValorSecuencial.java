package model.data_structures;

import java.util.Iterator;

import model.API.IListaLlaveValorSecuencial;

public class ListaLlaveValorSecuencial<K,V> implements IListaLlaveValorSecuencial<K, V>, Iterable<K>
{

	private int tamanio;
	
	private NodoSencillo <K , V> primero;
	private NodoSencillo< K , V> ultimo;

	// --------------------------------------------------------------------------------------------------------------------------------------------
	// Constructor.
	// --------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * Crea la lista vac�a.
	 */
	public ListaLlaveValorSecuencial() {
		// TODO Auto-generated constructor stub
		tamanio = 0;
		primero = null;
	}



	// --------------------------------------------------------------------------------------------------------------------------------------------
	// Metodos.
	// --------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	public int darTamanio() {
		// TODO Auto-generated method stub
		return tamanio;
	}

	@Override
	public boolean estaVacia() {
		// TODO Auto-generated method stub
		return tamanio == 0;
	}

	@Override
	public boolean tieneLlave(K llave) {
		// TODO Auto-generated method stub
		boolean encontrado = false;
		for(K key : this){
			if(key.equals(llave)){
				encontrado = true;
				break;
			}
		}

		return encontrado;

	}

	@Override
	public V darValor(K llave) {
		// TODO Auto-generated method stub
		NodoSencillo<K, V> actual = primero;
		while(actual != null && !actual.key.equals(llave)){
			actual = actual.next;
		}

		V valorBuscado = actual != null ? actual.value : null;

		return valorBuscado;
	}

	@Override
	public void insertar(K llave, V valor) {
		// TODO Auto-generated method stub
		NodoSencillo<K, V> nodoAgregar = new NodoSencillo<K , V>(valor, llave);

		NodoSencillo<K, V> actual = primero;
		NodoSencillo<K, V> anterior = null;

		while(actual != null && !actual.key.equals(llave)){
			anterior = actual;
			actual = actual.next;
		}

		if(primero == null){
			primero = ultimo =  nodoAgregar;
		}

		else if(actual == null){
			ultimo.next = nodoAgregar;
			ultimo = nodoAgregar;
		}

		else{
			actual.value = valor;
			tamanio--;
		}

		tamanio++;

	}
	@Override
	public Iterable<K> llaves() {
		// TODO Auto-generated method stub
		return  (Iterable<K>) new MiIterador<K , V>(primero);
	}


	// --------------------------------------------------------------------------------------------------------------------------------------------
	// Clase privada
	// --------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * Clase que modela los nodos de la lista.
	 * @param <K> llave adjunto al valor V-
	 * @param <V> valor a agregar a la clase
	 */

	private class NodoSencillo<K, V> {

		private V value;
		private K key;
		private NodoSencillo<K, V> next;

		public NodoSencillo (V pValue, K pKey){
			next = null;
			value = pValue;
			key = pKey;
		}

		public V getElement(){
			return value;
		}

		public K getKey(){
			return key; 
		}

		public NodoSencillo<K , V> getNext(){
			return next;
		}

		public void setNext(NodoSencillo<K, V> pNext){
			next = pNext;
		}

	}

	/**
	 * Clase que modela el iterador de un ListaLlaveValorSecuencial
	 * @param <T>
	 */

	private class MiIterador <K , V> implements Iterator<K>{

		NodoSencillo<K , V> actualIt;

		public MiIterador(NodoSencillo<K, V> pPrimero){
			actualIt = pPrimero;
		}

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return actualIt != null;
		}
		@Override
		public K next() {
			K key = actualIt.key;
			actualIt = actualIt.next;
			return key;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub

		}

	}

	@Override
	public Iterator<K> iterator() {
		// TODO Auto-generated method stub
		return new MiIterador<K , V>(primero);
	}

	public static void main(String[] args) {
		ListaLlaveValorSecuencial<Integer, String> nodoHash = new ListaLlaveValorSecuencial<>();
		nodoHash.insertar(0, "Hola");
		nodoHash.insertar(0, "HolaX2");
		nodoHash.insertar(1, "Camion");
		nodoHash.insertar(2, "FDGDFGn");
		nodoHash.insertar(3, "�APAn");
		nodoHash.insertar(4, "on");
			
	

		System.out.println(nodoHash.darValor(0));
		System.out.println(nodoHash.darValor(4));
		System.out.println(nodoHash.darTamanio());
	}

}

