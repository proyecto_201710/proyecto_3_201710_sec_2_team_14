package model.data_structures;

import java.util.Arrays;
import java.util.Iterator;

import model.API.IEncadenamientoSeparadoTH;


public class EncadenamientoSeparadoTH<K,V> implements IEncadenamientoSeparadoTH<K, V>, Iterable<K>{

	// --------------------------------------------------------------------------------------------------------------------------------------------
	// Atributos.
	// --------------------------------------------------------------------------------------------------------------------------------------------

	private ListaLlaveValorSecuencial<K, V>[] nodos;

	int elementosAgregados;
	int numListas;



	// --------------------------------------------------------------------------------------------------------------------------------------------
	// Constructor.
	// --------------------------------------------------------------------------------------------------------------------------------------------
	/**
	 * Crea la tabla de hash de encadenamiento separado con m posiciones (slots) iniciales.
	 */
	public EncadenamientoSeparadoTH(int m) {
		// TODO Auto-generated constructor stub
		numListas = m;
		nodos = (ListaLlaveValorSecuencial<K, V>[]) new ListaLlaveValorSecuencial[m];



	}


	// --------------------------------------------------------------------------------------------------------------------------------------------
	// Metodos.
	// --------------------------------------------------------------------------------------------------------------------------------------------

	public void clean(){
		nodos = (ListaLlaveValorSecuencial<K, V>[]) new ListaLlaveValorSecuencial[numListas];
		elementosAgregados = 0;
		
	}

	@Override
	public double darFactorCarga() {
		// TODO Auto-generated method stub
		return (((double)elementosAgregados)/numListas);
	}	
	@Override
	public int darTamanio() {
		// TODO DONE Auto-generated method stub
		return numListas;
	}

	public int darElementosAgregados(){
		return elementosAgregados; 
	}

	@Override
	public boolean estaVacia() {
		// TODO Auto-generated method stub
		return elementosAgregados==0;
	}

	public ListaLlaveValorSecuencial<K, V>[] darNodos(){
		return nodos;
	}

	@Override
	public boolean tieneLlave(K llave) {
		// TODO Auto-generated method stub
		try{
			ListaLlaveValorSecuencial<K, V> nodo = nodos[hash(llave)];
			if(nodo != null)
				return nodo.tieneLlave(llave);
		}

		catch(Exception e){
		}

		return false;

	}

	@Override
	public V darValor(K llave) {
		// TODO Auto-generated method stub
		V value = null;
		try{
			ListaLlaveValorSecuencial<K, V> nodo = nodos[hash(llave)];

			value = nodo.darValor(llave);

		}

		catch(Exception e){
			return null;
		}

		return value;
	}

	@Override
	public void insertar(K llave, V valor) {
		// TODO Auto-generated method stub
		try{

			if(darFactorCarga() >= 1.0)
				reHash();
			
			ListaLlaveValorSecuencial<K, V> nodo = nodos[hash(llave)];

			if(nodo != null){
				int tamanioAntes = nodo.darTamanio();
				nodo.insertar(llave, valor);
				int tamanioNodo = nodo.darTamanio();

				if(tamanioNodo - tamanioAntes > 0)
					elementosAgregados++;
			}

			else{
				nodos[hash(llave)] = new ListaLlaveValorSecuencial<K, V>();
				nodos[hash(llave)].insertar(llave, valor);
				elementosAgregados++;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}




	@Override
	public int hash(K llave) {
		// TODO Auto-generated method stub
			
		return (llave.hashCode()& 0x7fffffff) % darTamanio();
	}

	@Override
	public int[] darLongitudListas() {
		// TODO Auto-generated method stub
		int[] longitudes = new int[nodos.length];
		int pos = 0;
		for(ListaLlaveValorSecuencial<K, V> nodo : nodos)
			longitudes[pos++] = nodo.darTamanio();


		return longitudes;
	}


	@Override
	public Iterator<K> iterator() {
		// TODO Auto-generated method stub
		return new MiIterador<>();
	}


	@Override
	public void reHash() {
		EncadenamientoSeparadoTH<K, V> nuevaTabla = new EncadenamientoSeparadoTH<>(0);
		if(darFactorCarga() >= 1.0)
			nuevaTabla = new EncadenamientoSeparadoTH<>(numListas*2);

		//		else if(darFactorCarga() <= 0.1)
		//			nuevaTabla = new EncadenamientoSeparadoTH<>(numListas/2);

		for(K key: this)
			nuevaTabla.insertar(key, darValor(key));

		nodos = nuevaTabla.darNodos();
		numListas = nuevaTabla.numListas;
		elementosAgregados = nuevaTabla.elementosAgregados;


	}


	@Override
	public Iterable<K> llaves() {
		// TODO Auto-generated method stub
		return (Iterable<K>) new MiIterador();
	}



	private class MiIterador <K , V> implements Iterator<K>{

		int posActual = 0;
		Iterator iteratorActual;


		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			while(posActual < nodos.length)
			{
				
				if(iteratorActual == null || !iteratorActual.hasNext())
					 iteratorActual = nodos[posActual++] != null ? nodos[posActual-1].iterator(): null;
			

				if(iteratorActual != null && iteratorActual.hasNext())
					return true;
				
			
			}

			return false;
		}
		@Override
		public K next() {
			return (K) iteratorActual.next();
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub

		}

	}

	public static void main(String[] args) {
		EncadenamientoSeparadoTH<Integer, String> tablaHash = new EncadenamientoSeparadoTH<>(4);

		tablaHash.insertar(0, "0");
		tablaHash.insertar(1, "1");
		System.out.println("FC: " + tablaHash.darFactorCarga());
		tablaHash.insertar(2, "2");
		tablaHash.insertar(10, "10");
		System.out.println("NumListas: " + tablaHash.darTamanio());
		tablaHash.insertar(3, "3");
		System.out.println("NumListas: " + tablaHash.darTamanio());

		for(Integer key: tablaHash){
			System.out.println(tablaHash.darValor(key));
		}



	}



}
