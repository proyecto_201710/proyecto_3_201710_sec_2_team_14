package model.data_structures;

public class NodoCamino <K> {

	private K idVertice;
	private K idVerticePostor;
	private double costoTotal;
	private int longitud;


	public NodoCamino(K idVertice, K idVerticePostor, double costoTotal,
			int longitud) {
		
		this.idVertice = idVertice;
		this.idVerticePostor = idVerticePostor;
		this.costoTotal = costoTotal;
		this.longitud = longitud;
	}
	public K getIdVertice() {
		return idVertice;
	}
	public void setIdVertice(K idVertice) {
		this.idVertice = idVertice;
	}
	public K getIdVerticePostor() {
		return idVerticePostor;
	}
	public void setIdVerticePostor(K idVerticePostor) {
		this.idVerticePostor = idVerticePostor;
	}
	public double getCostoTotal() {
		return costoTotal;
	}
	public void setCostoTotal(double costoTotal) {
		this.costoTotal = costoTotal;
	}
	public int getLongitud() {
		return longitud;
	}
	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Id: " + idVertice + " Propuesto por: " + idVerticePostor + " Costo: " + costoTotal + " Longitud: " + longitud; 
	}



}
