package model.data_structures;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import model.API.ILista;
import model.API.IWeightedDirectedGraph;

public class WeightedDirectedGraph <K, V> implements IWeightedDirectedGraph<K, V>
{
	EncadenamientoSeparadoTH<K, NodoGrafo<K, V>> vertices;
	EncadenamientoSeparadoTH<K, ListaDobleEncadenada<Arco<K, V>>> arcos;

	public WeightedDirectedGraph() {
		// TODO Auto-generated constructor stub
		vertices = new EncadenamientoSeparadoTH<>(10);
		arcos = new EncadenamientoSeparadoTH<>(10);
	}
	@Override
	public int numVertices() {
		// TODO Auto-generated method stub
		return vertices.elementosAgregados;
	}

	@Override
	public V darVertice(K id) throws NoSuchElementException {
		// TODO Auto-generated method stub
		NodoGrafo<K, V> nodoBuscado = vertices.darValor(id);
		if(nodoBuscado == null)
			throw new NoSuchElementException("No se encontr� un nodo con el id: " + id);

		return nodoBuscado.valor;
	}

	@Override
	public void agregarVertice(K id, V indoVer) {
		// TODO Auto-generated method stub
		vertices.insertar(id, new NodoGrafo<K, V>(id, indoVer));
		arcos.insertar(id, new ListaDobleEncadenada<Arco<K, V>>());

	}

	@Override
	public int numArcos() {
		// TODO Auto-generated method stub
		int numArcos = 0;
		for(K key : arcos)
			numArcos += arcos.darValor(key).darNumeroElementos();
		return numArcos;
	}

	@Override
	public double darPesoArco(K idOrigen, K idDestino) throws NoSuchElementException {
		// TODO Auto-generated method stub
		double peso = 0.0;
		for(Arco<K , V> arco: arcos.darValor(idOrigen)){

			if(arco.verticeDestino.id.equals(idDestino)){
				peso = arco.peso;
				return peso;
			}
		}

		throw new NoSuchElementException("No existe un arco entre: " + idOrigen + " y " + idDestino + ".");
	}

	@Override
	public void agregarArco(K idOrigen, K idDestino, double peso) {
		// TODO Auto-generated method stub
		arcos.darValor(idOrigen).agregarElementoFinal(new Arco<K, V>(vertices.darValor(idDestino), peso));

	}

	@Override
	public void agregarArco(K idOrigen, K idDestino, double peso, char ordenLexicografico) {
		// TODO Auto-generated method stub
		Arco<K, V> arcoAgregar = new Arco<K, V>(vertices.darValor(idDestino), peso);
		arcoAgregar.setOrdenLexicografico(ordenLexicografico);
		arcos.darValor(idOrigen).agregarElementoFinal(arcoAgregar);
	}

	@Override
	public Iterator<K> darVertices() {
		// TODO Auto-generated method stub
		return vertices.iterator();
	}

	@Override
	public int darGrado(K id) {
		// TODO Auto-generated method stub
		return arcos.darValor(id).darNumeroElementos();
	}

	@Override
	public Iterator<K> darVerticesAdyacentes(K id) {
		// TODO Auto-generated method stub
		// Notese que los vertices adyacentes se ordenan por orden lexicografico

		ListaDobleEncadenada<K> verticesPorId = new ListaDobleEncadenada<>();

		arcos.darValor(id).sort(new Comparator<Arco<K,V>>() {

			@Override
			public int compare(Arco<K, V> arco1, Arco<K, V> arco2) {
				// TODO Auto-generated method stub

				if(arco1.ordenLexicografico == null && arco2.ordenLexicografico == null)
					return 0;
				else if(arco1.ordenLexicografico == null)
					return 1;
				else if(arco2.ordenLexicografico == null)
					return -1;
				else
					return arco1.ordenLexicografico.compareTo(arco2.ordenLexicografico);
			}
		});

		for(Arco<K, V> arcoActual : arcos.darValor(id))
			verticesPorId.agregarElementoFinal(arcoActual.verticeDestino.id);



		return verticesPorId.iterator();
	}

	@Override
	public void desmarcar() {
		// TODO Auto-generated method stub
		for(K idGrafo : vertices)
			vertices.darValor(idGrafo).marcar(false);

	}

	@Override
	public NodoCamino<K>[] DFS(K idOrigen) {
		// TODO Auto-generated method stub
		desmarcar();
		ListaDobleEncadenada<NodoCamino<K>> recorridoDFS = new ListaDobleEncadenada<>();
		
		DFSRecursivo(idOrigen, idOrigen, 0, 0.0, (ILista<NodoCamino<K>>) recorridoDFS);
		
		NodoCamino<K>[] respuesta = new NodoCamino[recorridoDFS.darNumeroElementos()];
		int i = 0;
		
		for(NodoCamino<K> nodo : recorridoDFS)
			respuesta[i++] = nodo;
		
		return respuesta;
	}
	
	private void DFSRecursivo(K idActual, K idPostor, Integer nivel, Double pesoAcumulado, ILista<NodoCamino<K>> camino){
		//Apenas inicie el recorrido, marca al vertice
		vertices.darValor(idActual).marcar(true);
		Iterator<K> verticesAdyacentes = darVerticesAdyacentes(idActual);
		
		//Obtener el peso del anterior al actual
		double costoDesdeAnterior = 0;
		
		for(Arco<K, V> arcoActual : arcos.darValor(idPostor)){
			if(arcoActual.verticeDestino.id.equals(idActual)){
				costoDesdeAnterior = arcoActual.peso;
				break;
			}
		}
		
		if(verticesAdyacentes.hasNext())
			while(verticesAdyacentes.hasNext()){
				K verticeAdyacente = verticesAdyacentes.next(); 
				if(vertices.darValor(verticeAdyacente).marca == true)
					continue;
				
				DFSRecursivo(verticeAdyacente, idActual, nivel + 1, pesoAcumulado + costoDesdeAnterior, camino);
			}
			
		camino.agregarElementoFinal(new NodoCamino<K>(idActual, idPostor, pesoAcumulado + costoDesdeAnterior, nivel));
		
		
	}

	@Override
	public NodoCamino<K>[] BFS(K idOrigen) {
		// TODO Auto-generated method stub

		//Desmarcar todos los vertices
		desmarcar();
		int nivel = 0;
		ListaDobleEncadenada<NodoCamino<K>> recorridoBFSAux = new ListaDobleEncadenada<>();
		Queue<NodoCamino<K>> cola = new Queue<>();
		Queue<NodoCamino<K>> colaSiguienteNivel = new Queue<>();

		//Encolar la raiz y marcarlo
		vertices.darValor(idOrigen).marcar(true);
		cola.enqueue(new NodoCamino<>(idOrigen, idOrigen, 0, 0));

		while(!cola.isEmpty()){
			nivel++;

			//Se sacan los elementos de la cola, y luego de sacarlos a todos, se agregan los del siguiente 
			//nivel
			while(!cola.isEmpty()){
				NodoCamino<K> elementoActual = cola.dequeue();
				recorridoBFSAux.agregarElementoFinal(elementoActual);
				Iterator<K> iteratorAdyacentes = darVerticesAdyacentes(elementoActual.getIdVertice());

				//Se insertar los elementos del proximo nivel
				while(iteratorAdyacentes.hasNext()){
					K adyacenteActual = iteratorAdyacentes.next();

					//Si el vertice ya estaba marcado, se ignora.
					if(vertices.darValor(adyacenteActual).marca == true){
						continue;
					}

					double costoDesdeAnterior = 0.0;

					//Obtener el costo desde el elemento actual
					for(Arco<K, V> arcoActual : arcos.darValor(elementoActual.getIdVertice())){
						if(arcoActual.verticeDestino.id.equals(adyacenteActual)){
							costoDesdeAnterior = arcoActual.peso;
							break;
						}
					}

					//Marca el vertice y lo encola
					vertices.darValor(adyacenteActual).marcar(true);
					colaSiguienteNivel.enqueue(new NodoCamino<K>(adyacenteActual, elementoActual.getIdVertice(), elementoActual.getCostoTotal() + costoDesdeAnterior, nivel));	
				}
			}

			cola = colaSiguienteNivel;
		}

		//Remplazar la lista encadenada por el arreglo correspondiente
		NodoCamino<K>[] respuesta = new NodoCamino[recorridoBFSAux.darNumeroElementos()];
		int i = 0;
		for(NodoCamino<K> nodo : recorridoBFSAux)
			respuesta[i++] = nodo;

		return respuesta;
	}

	@Override
	public NodoCamino<K>[] DFS(K idOrigen, K idDestino) {
		desmarcar();
		boolean [] termino = {false}; 
		
		ListaDobleEncadenada<NodoCamino<K>> recorridoDFS = new ListaDobleEncadenada<>();
		DFSRecursivoConFinal(idOrigen, idOrigen, 0, 0.0, (ILista<NodoCamino<K>>) recorridoDFS, termino, idDestino);
		if(!termino[0])
			return null;
		
		NodoCamino<K>[] respuesta = new NodoCamino[recorridoDFS.darNumeroElementos()];
		int i = 0;
		
		for(NodoCamino<K> nodo : recorridoDFS)
			respuesta[i++] = nodo;
		
		return respuesta;
	}
	
	private void DFSRecursivoConFinal(K idActual, K idPostor, Integer nivel, Double pesoAcumulado, ILista<NodoCamino<K>> camino, boolean[] termino, K idDestino){
		
		//Apenas inicie el recorrido, marca al vertice
		vertices.darValor(idActual).marcar(true);
		Iterator<K> verticesAdyacentes = darVerticesAdyacentes(idActual);
		
		//Obtener el peso del anterior al actual
		double costoDesdeAnterior = 0;
		
		for(Arco<K, V> arcoActual : arcos.darValor(idPostor)){
			if(arcoActual.verticeDestino.id.equals(idActual)){
				costoDesdeAnterior = arcoActual.peso;
				break;
			}
		}
		
		if(verticesAdyacentes.hasNext() && !termino[0])
			while(verticesAdyacentes.hasNext()){
				K verticeAdyacente = verticesAdyacentes.next(); 
				if(vertices.darValor(verticeAdyacente).marca == true)
					continue;
				
				DFSRecursivoConFinal(verticeAdyacente, idActual, nivel + 1, pesoAcumulado + costoDesdeAnterior, camino, termino, idDestino);
			}
			
		if(!termino[0])
		camino.agregarElementoFinal(new NodoCamino<K>(idActual, idPostor, pesoAcumulado + costoDesdeAnterior, nivel));
		
		if(idActual == idDestino)
			termino[0] = true;
		
		
	}

	@Override
	public NodoCamino<K>[] BFS(K idOrigen, K idDestino) {
		// TODO Auto-generated method stub

		//Desmarcar todos los vertices
		desmarcar();

		int nivel = 0;
		ListaDobleEncadenada<NodoCamino<K>> recorridoBFSAux = new ListaDobleEncadenada<>();
		Queue<NodoCamino<K>> cola = new Queue<>();
		Queue<NodoCamino<K>> colaSiguienteNivel = new Queue<>();

		//Encolar la raiz y marcarlo
		vertices.darValor(idOrigen).marcar(true);
		cola.enqueue(new NodoCamino<>(idOrigen, idOrigen, 0, 0));
		boolean encontroDestino = false;

		while(!cola.isEmpty() && !encontroDestino){
			nivel++;

			//Se sacan los elementos de la cola, y luego de sacarlos a todos, se agregan los del siguiente 
			//nivel
			while(!cola.isEmpty() && !encontroDestino){
				NodoCamino<K> elementoActual = cola.dequeue();
				recorridoBFSAux.agregarElementoFinal(elementoActual);
				
				if(elementoActual.getIdVertice().equals(idDestino)){
					encontroDestino = true;
				}
				Iterator<K> iteratorAdyacentes = darVerticesAdyacentes(elementoActual.getIdVertice());

				//Se insertar los elementos del proximo nivel
				while(iteratorAdyacentes.hasNext() && !encontroDestino){
					K adyacenteActual = iteratorAdyacentes.next();

					//Si el vertice ya estaba marcado, se ignora.
					if(vertices.darValor(adyacenteActual).marca == true)
						continue;
					


					double costoDesdeAnterior = 0.0;

					//Obtener el costo desde el elemento actual
					for(Arco<K, V> arcoActual : arcos.darValor(elementoActual.getIdVertice())){
						if(arcoActual.verticeDestino.id.equals(adyacenteActual)){
							costoDesdeAnterior = arcoActual.peso;
							break;
						}
					}

					//Marca el vertice y lo encola
					vertices.darValor(adyacenteActual).marcar(true);
					colaSiguienteNivel.enqueue(new NodoCamino<K>(adyacenteActual, elementoActual.getIdVertice(), elementoActual.getCostoTotal() + costoDesdeAnterior, nivel));
					
				}
			}

			cola = colaSiguienteNivel;
		}

		if(!encontroDestino)
			return null;
		//Remplazar la lista encadenada por el arreglo correspondiente
		NodoCamino<K>[] respuesta = new NodoCamino[recorridoBFSAux.darNumeroElementos()];
		int i = 0;
		for(NodoCamino<K> nodo : recorridoBFSAux)
			respuesta[i++] = nodo;

		return respuesta;
	}

	private class NodoGrafo <K, V>{
		K id;
		V valor;
		boolean marca;

		public NodoGrafo(K id, V value) {
			this.id =id;
			this.valor = value;
			marca = false;

		}

		public void marcar(boolean marca){
			this.marca = marca;
		}
	}

	private class Arco <K, V>{
		NodoGrafo <K, V> verticeDestino;
		double peso;
		Character ordenLexicografico;

		public Arco(NodoGrafo<K, V> pVerticeDestino, double pPeso){
			verticeDestino = pVerticeDestino;
			peso = pPeso;
			ordenLexicografico = null;
		}

		public void setOrdenLexicografico(Character letra){
			ordenLexicografico = letra;
		}


	}

	public static void main(String[] args) {
		WeightedDirectedGraph<Integer, Integer> grafo = new WeightedDirectedGraph<>();
		for(int i = 1; i < 10; i++)
			grafo.agregarVertice(i, i);
		
		grafo.agregarVertice(10, 10);
		
		grafo.agregarArco(1, 5, 4, 'a');
		grafo.agregarArco(2, 9, 4, 'a');
		grafo.agregarArco(2, 4, 1, 'b');
		grafo.agregarArco(3, 1, 2, 'a');
		grafo.agregarArco(3, 5, 4, 'b');
		grafo.agregarArco(4, 8, 3, 'a');
		grafo.agregarArco(5, 7, 5, 'a');
		grafo.agregarArco(5, 2, 8, 'b');
		grafo.agregarArco(5, 4, 8, 'c');
		grafo.agregarArco(5, 6, 5, 'd');
		grafo.agregarArco(6, 4, 4, 'a');
		grafo.agregarArco(6, 8, 6, 'b');
		grafo.agregarArco(6, 3, 6, 'c');
		grafo.agregarArco(7, 9, 6, 'a');
		grafo.agregarArco(7, 2, 1, 'b');
		grafo.agregarArco(7, 1, 10, 'c');
		grafo.agregarArco(8, 3, 4, 'a');
		grafo.agregarArco(9, 1, 4, 'a');
		
		
		System.out.println(Arrays.toString(grafo.DFS(5)));
		System.out.println(Arrays.toString(grafo.BFS(5)));
		System.out.println(Arrays.toString(grafo.BFS(5, 2)));
		System.out.println(Arrays.toString(grafo.DFS(5, 1)));
		


	}



}
