package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Iterator;
import java.nio.file.*;
import java.sql.Time;

import javax.json.Json;
import javax.json.JsonObject;

import com.google.gson.JsonArray;
import com.google.gson.JsonStreamParser;
import com.google.gson.stream.JsonReader;

import model.API.IEdge;
import model.API.ILista;
import model.API.ISistemaRecomendacion;
import model.VO.VOFranquicia;
import model.VO.VOGeneroPelicula;
import model.VO.VOPelicula;
import model.VO.VOPeliculaPlan;
import model.VO.VOTeatro;
import model.VO.VOUbicacion;
import model.VO.VOUsuario;
import model.data_structures.EncadenamientoSeparadoTH;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.WeightedDirectedGraph;

public class SistemaRecomendacion implements ISistemaRecomendacion{

	EncadenamientoSeparadoTH<String, VOTeatro> teatros;
	WeightedDirectedGraph <String, VOTeatro> redDeteatros;
	EncadenamientoSeparadoTH<Integer, VOPelicula> peliculas;
	EncadenamientoSeparadoTH<String, EncadenamientoSeparadoTH<String, ListaDobleEncadenada<VOPeliculaPlan>>> [] programacion;

	public SistemaRecomendacion() {
		// TODO Auto-generated constructor stub
		redDeteatros = new WeightedDirectedGraph<>();
		teatros = new EncadenamientoSeparadoTH<>(50);
		peliculas = new EncadenamientoSeparadoTH<>(100);
		programacion = new EncadenamientoSeparadoTH[5];

	}

	@Override
	public ISistemaRecomendacion crearSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean cargarTeatros(String ruta) {
		// TODO Auto-generated method stub
		boolean teatrosCargados = false;

		try{
			String json = new String(Files.readAllBytes(Paths.get(ruta)));
			json = json.substring(3);

			JsonReader jsonReader = new JsonReader(new StringReader(json));


			jsonReader.beginArray();

			while(jsonReader.hasNext()){
				VOTeatro teatro = new VOTeatro();
				jsonReader.beginObject();

				while(jsonReader.hasNext()){

					String atributo = jsonReader.nextName();

					//Nombre de cada teatro
					if(atributo.equals("Nombre"))
						teatro.setNombre(jsonReader.nextString());

					else if(atributo.equals("Ubicacion geografica (Lat|Long)")){
						VOUbicacion ubicacion = new VOUbicacion();
						String[] latLong = jsonReader.nextString().split("\\|");

						double lat = Double.parseDouble(latLong[0]);
						double longit = Double.parseDouble(latLong[1]);

						ubicacion.setLatitud(lat);
						ubicacion.setLongitud(longit);

						teatro.setUbicacion(ubicacion);
					}
					//Franquicia
					else if(atributo.equals("Franquicia")){
						VOFranquicia franquicia = new VOFranquicia();
						franquicia.setNombre(jsonReader.nextString());

						teatro.setFranquicia(franquicia);
					}


				}

				teatros.insertar(teatro.getNombre(), teatro);
				jsonReader.endObject();
			}

			jsonReader.endArray();

			//Programación

			teatrosCargados = true;


		} catch(Exception e){
			e.printStackTrace();
		}

		return teatrosCargados;
	}

	@Override
	public boolean cargarCartelera(String ruta) {
		// TODO Auto-generated method stub
		try{

			//Cargar de peliculas
			String json = new String(Files.readAllBytes(Paths.get("./data/movies_filtered.json")));
			json = json.substring(3);

			JsonReader jsonReader = new JsonReader(new StringReader(json));


			jsonReader.beginArray();

			while(jsonReader.hasNext()){
				jsonReader.beginObject();

				int idPelicula = 0;
				String titulo = "";
				String [] generos = null;

				while(jsonReader.hasNext()){
					String atributo = jsonReader.nextName();
					String texto = jsonReader.nextString();

					if(atributo.equals("movie_id"))
						idPelicula = Integer.parseInt(texto);

					else if(atributo.equals("title"))
						titulo = texto;


					else if(atributo.equals("genres"))
						generos = texto.split("\\|");
				}

				VOPelicula peli = new VOPelicula(idPelicula, titulo, generos);
				peliculas.insertar(peli.getId(), peli);

				jsonReader.endObject();
			}

			jsonReader.endArray();
			jsonReader.close();

			//Cargar programación
			String dia = "./data/Programacion-v3/dia";
			for(int i = 1; i <= 5; i++){
				programacion[i-1] = new EncadenamientoSeparadoTH<>(teatros.darElementosAgregados()+1);
				json = new String(Files.readAllBytes(Paths.get(dia+i+".json")));


				javax.json.JsonReader lector = Json.createReader(new StringReader(json));
				javax.json.JsonArray teatros = lector.readArray();


				for(Object teatroI: teatros){

					javax.json.JsonObject teatro = (JsonObject) teatroI;
					String nombreTeatro = teatro.getString("teatros");

					EncadenamientoSeparadoTH<String, ListaDobleEncadenada<VOPeliculaPlan>> franjasHorarias = new EncadenamientoSeparadoTH<>(20);

					javax.json.JsonObject peliculasObjeto = teatro.getJsonObject("teatro");
					javax.json.JsonArray peliculasJson = peliculasObjeto.getJsonArray("peliculas");

					for(Object peliculaI: peliculasJson){
						javax.json.JsonObject pelicula = (javax.json.JsonObject) peliculaI;


						int idPeli = pelicula.getInt("id");


						javax.json.JsonArray funciones = pelicula.getJsonArray("funciones");


						for(Object horaI: funciones){
							String hora = ((JsonObject)horaI).getString("hora");

							//Se crea la pelicula de cierta franja horaria
							VOPeliculaPlan peliculaPlan = new VOPeliculaPlan();
							peliculaPlan.setDia(i);
							peliculaPlan.setPelicula(peliculas.darValor(idPeli));
							peliculaPlan.setHoraInicio(obtenerFecha(hora));

							//Verifica si la franja horario ya fue agregada a la tabla de cada dia
							if(franjasHorarias.tieneLlave(hora))
								franjasHorarias.darValor(hora).agregarElementoFinal(peliculaPlan);

							else{
								ListaDobleEncadenada<VOPeliculaPlan> pelisPorFranja = new ListaDobleEncadenada<>();
								pelisPorFranja.agregarElementoFinal(peliculaPlan);
								franjasHorarias.insertar(hora, pelisPorFranja);
							}
						}
					}




					programacion[i-1].insertar(nombreTeatro, franjasHorarias);
				}

			}

		} catch(Exception e){
			e.printStackTrace();

		}

		return false;
	}

	private Time obtenerFecha(String horaString){
		String [] aux = horaString.split(":");
		int hora = Integer.parseInt(aux[0]);
		if(horaString.contains("p.m.") && hora != 12)
			hora += 12;

		return new Time(hora, 0, 0);

	}

	@Override
	public boolean cargarRed(String ruta) {
		// TODO Auto-generated method stub
		try{
			//Leer archivo de red de teatros
			String json = new String(Files.readAllBytes(Paths.get(ruta)));
			json = json.substring(3);

			//Agrega todo los teatros a la red como vertices
			for(String nomTeatro: teatros){
				VOTeatro teatro = teatros.darValor(nomTeatro);
				redDeteatros.agregarVertice(nomTeatro, teatro);
			}

			//Se van a recorrer el archivo para agregar los arcos
			javax.json.JsonReader reader = Json.createReader(new StringReader(json));
			javax.json.JsonArray arcos = reader.readArray();

			for(Object arcoI: arcos){
				javax.json.JsonObject arcoJson = (javax.json.JsonObject) arcoI;
				String origen = arcoJson.getString("Teatro 1");
				String destino = arcoJson.getString("Teatro 2");
				Double peso = arcoJson.getJsonNumber("Tiempo (minutos)").doubleValue();

				redDeteatros.agregarArco(origen, destino, peso);
				redDeteatros.agregarArco(destino, origen, peso);
			}


		} catch(Exception e){
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public int sizeMovies() {
		// TODO Auto-generated method stub
		return peliculas.darElementosAgregados();
	}

	@Override
	public int sizeTeatros() {
		// TODO Auto-generated method stub
		return teatros.darElementosAgregados();
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPeliculas(VOUsuario usuario, int fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGenero(VOGeneroPelicula genero, VOUsuario usuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorFranquicia(VOFranquicia franquicia, int fecha, String franja) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroYDesplazamiento(VOGeneroPelicula genero, int fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroDesplazamientoYFranquicia(VOGeneroPelicula genero, int fecha,
			VOFranquicia franquicia) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<IEdge<VOTeatro>> generarMapa() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<ILista<VOTeatro>> rutasPosible(VOTeatro origen, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {
		SistemaRecomendacion sr = new SistemaRecomendacion();
		sr.cargarTeatros("./data/teatros_v3.json");
		sr.cargarCartelera("./data/movies_filtered.json");
		sr.cargarRed("./data/tiemposIguales_v2.json");

		//Imprimir peliculas
		//		for(Integer peli: sr.peliculas){
		//			VOPelicula peliActual = sr.peliculas.darValor(peli);
		//			System.out.println(peliActual.toString());
		//		}

		//Imprimir teatros
		//		for(String nomTeatro: sr.teatros){
		//			VOTeatro teatro = sr.teatros.darValor(nomTeatro);
		//			System.out.println(teatro);
		//		}

		//Imprimir agenda de un dia
		//		for(String nomTeatro: sr.programacion[4]){
		//			EncadenamientoSeparadoTH<String, ListaDobleEncadenada<VOPeliculaPlan>> teatro = sr.programacion[0].darValor(nomTeatro);	
		//			System.out.println("*********************Nombre de teatro: " + nomTeatro + " *****************");
		//			
		//			for(String franjaHoraria: teatro){
		//				System.out.println("--------------"+ franjaHoraria + "------------");
		//				ListaDobleEncadenada<VOPeliculaPlan> peliculas = teatro.darValor(franjaHoraria);
		//				for(VOPeliculaPlan peli: peliculas)
		//					System.out.println(peli);
		//			}
		//		}

		//Pruebas carga red de teatros
//		System.out.println(sr.redDeteatros.darVertice("Cine Colombia Mercurio"));
//		System.out.println(sr.redDeteatros.darPesoArco("Cine Colombia Andino", "Procinal Tintal Plaza"));
//		
	}



}
