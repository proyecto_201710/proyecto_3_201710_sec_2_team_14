

import model.data_structures.*;
import junit.framework.TestCase;


public class TestEncadenamientoSeparado extends TestCase{

	EncadenamientoSeparadoTH<Integer, String> tablaHash;

	/**
	 * Escenario 1: Crea una tabla hash con 0 posiciones
	 */

	public void setupEscenario1(){
		tablaHash = new EncadenamientoSeparadoTH<>(0);
	}

	public void setupEscenario2(){
		tablaHash = new EncadenamientoSeparadoTH<>(1);
		tablaHash.insertar(1, "Don String");
	}

	public void setupEscenario3(){
		tablaHash = new EncadenamientoSeparadoTH<>(1);

		for(int i = 65; i < 76; i++)
			tablaHash.insertar(0, new Character((char) i).toString());
	}

	public void setupEscenario4(){
		tablaHash = new EncadenamientoSeparadoTH<>(26);

		for(int i = 65; i < 90; i++)
			tablaHash.insertar(i, new Character((char) i).toString());
	}

	public void testDarValor(){

		//Escenario 1
		setupEscenario1();
		assertNull("Esa posici�n no existe; por lo tanto, deber�a retornar null", tablaHash.darValor(0));

		//Escenario 2
		setupEscenario2();
		assertEquals("La cadena no es la esperada", tablaHash.darValor(1), "Don String");

		//Escenario 3
		setupEscenario3();
		assertEquals("La cadena no es la esperada", tablaHash.darValor(0), "K");

		//Escenario 4
		setupEscenario4();
		for(int i = 65; i < 90; i++){
			Character letra = (char) i;
			assertEquals("La letra no es la esperada", letra.toString(), tablaHash.darValor(i));
		}

	}

	public void testInsertar(){

		//Escenario 1

		//Escenario 2
		setupEscenario2();
		tablaHash.insertar(0, "Mss. String");
		assertEquals("La cadena no es la esperada", tablaHash.darValor(0), "Mss. String");

		tablaHash.insertar(1, "Don String (volvio)");
		assertEquals("La cadena no es la esperada", tablaHash.darValor(1), "Don String (volvio)");

		//Escenario 4
		setupEscenario4();
		for(int i = 97; i < 123; i++)
			tablaHash.insertar(i, new Character((char) i).toString());

		for(int i = 65, k = 97; i < 90; i++, k++){
			Character letra = (char) i;
			Character letra2 = (char) k;
			assertEquals("La letra no es la esperada", letra.toString(), tablaHash.darValor(i));
			assertEquals("La letra no es la esperada", letra2.toString(), tablaHash.darValor(k));
		}

	}
}
