
import java.util.Iterator;
import junit.framework.TestCase;
import model.data_structures.ListaDobleEncadenada;


public class ListaDobleEnlazadaTest extends TestCase {
	private ListaDobleEncadenada<String> listaDobleEncadenada;

	public void setupEscenario1(){

		listaDobleEncadenada = new ListaDobleEncadenada<>();
		listaDobleEncadenada.agregarElementoFinal("Primera Frase de lista seria");
		listaDobleEncadenada.agregarElementoFinal("Esto no es copy-paste");
		listaDobleEncadenada.agregarElementoFinal("Fin de la lista mas aburrida");
	}

	public void setupEscenario2(){
		listaDobleEncadenada = new ListaDobleEncadenada<>();
		listaDobleEncadenada.agregarElementoFinal("Soy el #1");
	}

	public void setupEscenario3(){
		listaDobleEncadenada = new ListaDobleEncadenada<>();
	}

	public void testPosicionesCorrectas()
	{
		setupEscenario1();
		assertEquals("El primer elemento de la lista no corresponde","Primera Frase de lista seria", listaDobleEncadenada.darElemento(0));
		assertEquals("El segundo elemento de la lista no corresponde","Esto no es copy-paste", listaDobleEncadenada.darElemento(1));
		assertEquals("El tercer elemento de la lista no corresponde","Fin de la lista mas aburrida", listaDobleEncadenada.darElemento(2));

		setupEscenario3();
		try{
			listaDobleEncadenada.darElementoPosicionActual();
			fail();
		}

		catch(Exception e){

		}
	}

	public void testDesplazamientos(){

		setupEscenario1();
		assertEquals("No deber�a ser posible desplazarse a la posici�n anterior",false, listaDobleEncadenada.retrocederPosicionAnterior());

		assertEquals("Deber�a ser posible desplazarse a la siguiente posici�n",true, listaDobleEncadenada.avanzarSiguientePosicion());
		assertEquals("Esto no es copy-paste", listaDobleEncadenada.darElementoPosicionActual());

		assertEquals("Deber�a ser posible desplazarse a la siguiente posici�n",true, listaDobleEncadenada.avanzarSiguientePosicion());
		assertEquals("Fin de la lista mas aburrida", listaDobleEncadenada.darElementoPosicionActual());

		assertEquals("No deber�a ser posible desplazarse a la siguiente posici�n",false, listaDobleEncadenada.avanzarSiguientePosicion());
	}

	public void testTamanios(){
		setupEscenario1();
		assertEquals("El tama�o deberia ser de 3 elementos", 3, listaDobleEncadenada.darNumeroElementos());

		setupEscenario2();
		assertEquals("El tama�o deberia ser de 1 elemento", 1, listaDobleEncadenada.darNumeroElementos());

		setupEscenario3();
		assertEquals("El tama�o deberia ser de 1 elemento", 0, listaDobleEncadenada.darNumeroElementos());
	}

	public void testeAgregarElementos(){
		setupEscenario3();
		listaDobleEncadenada.agregarElementoFinal("Nuevo String");
		listaDobleEncadenada.agregarElementoFinal("Nuevo String2");
		listaDobleEncadenada.agregarElementoFinal("Nuevo String3");
		assertEquals("El ultimo elemento agregado coincide", "Nuevo String3", listaDobleEncadenada.darElemento(2));

	}
	
	public void testIterator(){
		setupEscenario1();
		Iterator<String> it = listaDobleEncadenada.iterator();
		assertEquals("S� hay siguiente elemento", true, it.hasNext());
		assertEquals("El primer elemento no coincide" ,"Primera Frase de lista seria", it.next());
		
		assertEquals("S� hay siguiente elemento", true, it.hasNext());
		assertEquals("El segundo elemento no coincide" ,"Esto no es copy-paste", it.next());
		
		assertEquals("S� hay siguiente elemento", true, it.hasNext());
		assertEquals("El tercer elemento no coincide", "Fin de la lista mas aburrida", it.next());
		
		assertEquals("No hay siguiente elemento", false, it.hasNext());
		try{
			it.next();
			fail();
		}
		
		catch(Exception e){
			
		}
	}
}
