import model.data_structures.*;

import java.util.Arrays;

import org.junit.Test;

import junit.framework.TestCase;

public class WeightedDirectedGraphTest extends TestCase {

	WeightedDirectedGraph<Integer, Integer> grafo;
	
	public void setupEscenario1(){
		grafo = new WeightedDirectedGraph<>();
		for(int i = 1; i < 10; i++)
			grafo.agregarVertice(i, i);
		
		grafo.agregarVertice(10, 10);
		
		grafo.agregarArco(1, 5, 4, 'a');
		grafo.agregarArco(2, 9, 4, 'a');
		grafo.agregarArco(2, 4, 1, 'b');
		grafo.agregarArco(3, 1, 2, 'a');
		grafo.agregarArco(3, 5, 4, 'b');
		grafo.agregarArco(4, 8, 3, 'a');
		grafo.agregarArco(5, 7, 5, 'a');
		grafo.agregarArco(5, 2, 8, 'b');
		grafo.agregarArco(5, 4, 8, 'c');
		grafo.agregarArco(5, 6, 5, 'd');
		grafo.agregarArco(6, 4, 4, 'a');
		grafo.agregarArco(6, 8, 6, 'b');
		grafo.agregarArco(6, 3, 6, 'c');
		grafo.agregarArco(7, 9, 6, 'a');
		grafo.agregarArco(7, 2, 1, 'b');
		grafo.agregarArco(7, 1, 10, 'c');
		grafo.agregarArco(8, 3, 4, 'a');
		grafo.agregarArco(9, 1, 4, 'a');
		
	}
	@Test
	public void testDFS(){
		//Escenario
		setupEscenario1();
		
		String respuesta = "[Id: 1 Propuesto por: 9 Costo: 15.0 Longitud: 3, Id: 9 Propuesto por: 7 Costo: 11.0 Longitud: 2, Id: 3 Propuesto por: 8 Costo: 14.0 Longitud: 5, Id: 8 Propuesto por: 4 Costo: 10.0 Longitud: 4, Id: 4 Propuesto por: 2 Costo: 7.0 Longitud: 3, Id: 2 Propuesto por: 7 Costo: 6.0 Longitud: 2, Id: 7 Propuesto por: 5 Costo: 5.0 Longitud: 1, Id: 6 Propuesto por: 5 Costo: 5.0 Longitud: 1, Id: 5 Propuesto por: 5 Costo: 0.0 Longitud: 0]"; 
		assertEquals(Arrays.toString(grafo.DFS(5)), respuesta);
	}
	@Test
	public void testBFS(){
		setupEscenario1();
		String respuesta = "[Id: 5 Propuesto por: 5 Costo: 0.0 Longitud: 0, Id: 7 Propuesto por: 5 Costo: 5.0 Longitud: 1, Id: 2 Propuesto por: 5 Costo: 8.0 Longitud: 1, Id: 4 Propuesto por: 5 Costo: 8.0 Longitud: 1, Id: 6 Propuesto por: 5 Costo: 5.0 Longitud: 1, Id: 9 Propuesto por: 7 Costo: 11.0 Longitud: 2, Id: 1 Propuesto por: 7 Costo: 15.0 Longitud: 2, Id: 8 Propuesto por: 4 Costo: 11.0 Longitud: 2, Id: 3 Propuesto por: 6 Costo: 11.0 Longitud: 2]";
		assertEquals(Arrays.toString(grafo.BFS(5)), respuesta);
		
	}

}
