import junit.framework.TestCase;
import model.data_structures.*;

public class QueueTest extends TestCase{

	private Queue<String> fila;

	//---------------------------------------------------------------------------------//
	//---------------------------------SETUP_ESCENARIOS--------------------------------//
	//---------------------------------------------------------------------------------//

	/**
	 * Crea una fila vacia. 
	 */

	public void setupEscenario1(){
		fila = new Queue<>();

	}


	/**
	 * Crea una fila y agrega un elemento 
	 */

	public void setupEscenario2(){
		fila = new Queue<>();

		fila.enqueue("soy una cadena xDXdXdxXdXd");

	}

	/**
	 * Crea una fila y agrega cuatro elementos
	 */

	public void setupEscenario3(){
		fila = new Queue<>();

		fila.enqueue("No vamos a hacer la primera entrega a tiempo");
		fila.enqueue("No c, weno zi c pero no te wa a dezir");
		fila.enqueue("Ayy, muchas cosas wohooo");
		fila.enqueue("Yo necisito amor, comrpensi�n y ternura");
	}

	//---------------------------------------------------------------------------------//
	//---------------------------------M�TODOS_PRUEBAS--------------------------------//
	//---------------------------------------------------------------------------------//

	/**
	 * Prueba el m�todo que retorna el tama�o de las filas
	 */

	public void testSize(){
		setupEscenario1();
		assertEquals("El tama�o de la fila deber�a ser 0",0, fila.size());

		setupEscenario2();
		assertEquals("El tama�o de la fila deberia ser 1", 1, fila.size());

		setupEscenario3();
		assertEquals("E� tama�o de la fila deber�a ser 4", 4, fila.size());

	}

	/**
	 * Prueba el m�todo que informa si las filas estan vacias o no
	 */

	public void testIsEmpty(){
		setupEscenario1();
		assertEquals("La fila deber�a estar vacia",true, fila.isEmpty());

		setupEscenario2();
		assertEquals("La fila no deber�a estar vacia",false, fila.isEmpty());
		fila.dequeue();
		assertEquals("La fila deber�a estar vac�a ahora",true, fila.isEmpty());

		setupEscenario3();
		assertEquals("La fila no deber�a estar vacia",false, fila.isEmpty());
	}

	/**
	 * Prueba el m�todo que atienden al primer elemento que lleg� a la fila
	 */

	public void testDequeue(){
		setupEscenario1();
		try{
			fila.dequeue();
			fail();
		} catch(Exception e){

		}

		setupEscenario2();
		assertEquals("El elemento atendido de la pila no corresponde con el esperado", "soy una cadena xDXdXdxXdXd", fila.dequeue());

		setupEscenario3();
		assertEquals("El elemento atendido de la fila no corresponde con el esperado", "No vamos a hacer la primera entrega a tiempo", fila.dequeue());
		assertEquals("El elemento atendido de la fila no corresponde con el esperado", "No c, weno zi c pero no te wa a dezir", fila.dequeue());
		assertEquals("El elemento atendido de la fila no corresponde con el esperado", "Ayy, muchas cosas wohooo", fila.dequeue());
		assertEquals("El elemento atendido de la fila no corresponde con el esperado", "Yo necisito amor, comrpensi�n y ternura", fila.dequeue());



	}

	/**
	 * Prueba el m�todo que agrega elementos a la fila
	 */

	public void testEnqueue(){
		setupEscenario1();	
		fila.enqueue("Holi");
		assertEquals("El elemento agregado a la pila no corresponde con el esperado", "Holi", fila.dequeue());

		setupEscenario2();
		fila.enqueue("Esta no es otra tipica cadena");
		fila.enqueue("100% real no feik");

		assertEquals("El elemento agregado a la fila no corresponde con el esperado", "soy una cadena xDXdXdxXdXd", fila.dequeue());
		assertEquals("El elemento agregado a la fila no corresponde con el esperado", "Esta no es otra tipica cadena", fila.dequeue());
		assertEquals("El elemento agregado a la fila no corresponde con el esperado", "100% real no feik", fila.dequeue());



	}


}
